======
Yagura
======

Simple site monitoring web-app for small team.


Description
===========

Yagura is simple website monitoring kit based by Django framework.
Yagura provides website monitorings, alerts and status reportings.


Features
========

* Health-check report of websites


NOT Features
============

* User management(if use ``yagura.url`` , include ``django.contrib.auth`` )
* Notifications (Planned to implement)

Initial user
============

* Username: ``admin``
* Password: ``Yagura!!``
