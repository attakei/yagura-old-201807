from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'yagura.core'
